package models

import (
	"bitbucket.org/antondom/evchargerlibrary/db"
	"fmt"
)

type WhiteList struct {
	UserId int
	Token  string
}

func (wl *WhiteList) Find(userId int, tok string) bool {
	con := db.GetDBConnect()
	fmt.Println(tok)
	err := con.Get(wl, "SELECT * FROM whitelist WHERE userId = ? AND token = '"+tok+"' ", userId)
	db.CheckError(err)
	fmt.Println(err)
	fmt.Println(tok)
	if err != nil {
		return false
	} else {
		return true
	}
}

func (wl WhiteList) Update(token string) {
	con := db.GetDBConnect()
	_, err := con.Exec("UPDATE whitelist SET token = ? WHERE userid = ? AND token = ? ", token, wl.UserId, wl.Token)
	db.CheckError(err)
}

func (wl WhiteList) Delete() {
	con := db.GetDBConnect()
	_, err := con.Exec("DELETE FROM whitelist WHERE userid = ? AND token = ?", wl.UserId, wl.Token)
	db.CheckError(err)
}

func (wl WhiteList) DeleteByUserId(userId int) {
	con := db.GetDBConnect()
	_, err := con.Exec("DELETE FROM whitelist WHERE userid = ? ", userId)
	db.CheckError(err)
}

func (wl WhiteList) Insert() {
	con := db.GetDBConnect()
	_, err := con.Exec("INSERT INTO whitelist (userid,token) VALUES (?,?) ", wl.UserId, wl.Token)
	db.CheckError(err)
}
