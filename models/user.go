package models

import (
	"bitbucket.org/antondom/evchargerlibrary/db"
	"fmt"
	"strconv"
	"time"
)

type User struct {
	Id        int
	Login     string
	Password  string
	Email     string
	Role      string
	RegDate   string
	Company   string
	Longitude float64
	Latitude  float64
	Fio       string
	Phone     string
	Country   string
	City      string
	UTC       int
	Unn       string
	Car       string
	Language  string
	AvatarUrl string
	Balance   float64
	ParentId  int
}

type Users []User

func (users *Users) Find(parentId int) bool {
	con := db.GetDBConnect()
	err := con.Select(users, "SELECT * FROM users WHERE parentid = ? ", parentId)
	db.CheckError(err)

	if users == nil || len(*users) == 0 {
		return false
	} else {
		return true
	}
}

func (user *User) Find(login string) bool {
	con := db.GetDBConnect()
	err := con.Get(user, "SELECT * FROM users WHERE login = ? ", login)
	db.CheckError(err)

	if err != nil {
		return false
	} else {
		return true
	}
}

func (user *User) FindById(id int) bool {
	con := db.GetDBConnect()
	err := con.Get(user, "SELECT * FROM users WHERE id = ? ", id)
	db.CheckError(err)

	if err != nil {
		return false
	} else {
		return true
	}
}

func (user *User) FindByEmail(email string) bool {
	con := db.GetDBConnect()
	err := con.Get(user, "SELECT * FROM users WHERE email = ? ", email)
	db.CheckError(err)

	if err != nil {
		return false
	} else {
		return true
	}
}

func (u *User) Insert() {
	con := db.GetDBConnect()
	tm := time.Now()
	tm.Format(time.RFC3339)
	_, err := con.Exec("INSERT INTO users (login,password,email,role,regdate,company ,longitude,latitude,phone,car,language,avatarurl,parentid ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",
		u.Login, u.Password, u.Email, u.Role, tm, u.Company, u.Latitude, u.Longitude, u.Phone, u.Car, u.Language, u.AvatarUrl, u.ParentId)
	db.CheckError(err)
	u.Find(u.Login)
}

func (u *User) UpdatePassword(password string) {
	con := db.GetDBConnect()
	_, err := con.Exec("UPDATE users SET password = ? WHERE id = ?", password, u.Id)
	db.CheckError(err)
}

func (u *User) Delete() {
	con := db.GetDBConnect()
	_, err := con.Exec("DELETE FROM users WHERE id = ? ", u.Id)
	db.CheckError(err)
}

func (u *User) Update() {
	con := db.GetDBConnect()
	query := `UPDATE users SET fio = ?, phone = ?, country = ?, city = ?,
              utc = ?, unn = ?, company = ?, email = ?,longitude = ?,latitude = ?,car = ?,language = ?, avatarurl = ? WHERE id = ? `
	_, err := con.Exec(query, u.Fio, u.Phone, u.Country, u.City, u.UTC, u.Unn,
		u.Company, u.Email, u.Longitude, u.Latitude, u.Car, u.Language, u.AvatarUrl, u.Id)
	db.CheckError(err)
}

func (u *User) UpdateBalance() {
	fmt.Println("update user balance")
	con := db.GetDBConnect()
	query := `UPDATE users SET balance = ? WHERE id = ? `
	_, err := con.Exec(query, u.Balance, u.Id)
	db.CheckError(err)
}

func GetStationsCount(userId int) int {
	fmt.Println("userid = " + strconv.Itoa(userId))
	count := 0
	con := db.GetDBConnect()
	err := con.Get(&count, "SELECT count(*) as count FROM stationsaccess WHERE userid = ? ", userId)
	db.CheckError(err)
	return count
}
