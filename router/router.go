package router

import (
	"../middleware"
	"bitbucket.org/antondom/evchargerlibrary/encryption"
	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"
	"github.com/itsjamie/gin-cors"
	"time"
)

func New() *gin.Engine {
	r := gin.Default()

	config := cors.Config{
		Origins:         "*",
		RequestHeaders:  "Authorization, Content-Type",
		Methods:         "GET, POST, PUT",
		Credentials:     true,
		ValidateHeaders: false,
		MaxAge:          1 * time.Minute,
	}

	r.Use(cors.Middleware(config))

	r.POST("api/signIn", middleware.SignIn)
	r.POST("api/signUp", middleware.SignUp)
	r.POST("api/refresh", middleware.Refresh)
	r.POST("api/newPassword", middleware.NewPassword)
	r.POST("api/delete",encryption.Authorize, middleware.DeleteUser)

	r.POST("api/changePassword", encryption.Authorize, middleware.ChangePassword)
	r.POST("api/changeEmail", encryption.Authorize, middleware.ChangeEmail)
	r.POST("api/changeAvatar", encryption.Authorize, middleware.ChangeAvatar)
	r.POST("api/changeData", encryption.Authorize, middleware.ChangeData)
	r.POST("api/createNewUser", encryption.Authorize, middleware.CreateNewUser)

	r.GET("api/isLoginExists", middleware.IsLoginExists)
	r.GET("api/getData", encryption.Authorize, middleware.GetData)
	r.GET("api/getUsers", encryption.Authorize, middleware.GetUsers)

	r.Use(static.Serve("/static", static.LocalFile("./images/avatars", false)))
	return r
}
