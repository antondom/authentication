package middleware

import (
	"../models"
	"bitbucket.org/antondom/evchargerlibrary/config"
	"bitbucket.org/antondom/evchargerlibrary/email"
	"bitbucket.org/antondom/evchargerlibrary/encryption"
	"encoding/json"
	"fmt"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/copier"
	"golang.org/x/crypto/bcrypt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"
)

const (
	ErrorLoginEmpty          = 1
	ErrorPasswordEmpty       = 2
	ErrorEmailEmpty          = 3
	ErrorPasswordNotValid    = 4
	ErrorEmailExists         = 5
	ErrorLoginExists         = 6
	ErrorLoginNotValid       = 7
	ErrorLoginNotFound       = 8
	ErrorUserNotFound        = 9
	ErrorOldPasswordNotValid = 10
	ErrorSendEmail           = 11
	ErrorNoPermission        = 12
	ErrorWrongRole           = 13
)

type SignUpRequest struct {
	Login     string  `json:"login"`
	Password  string  `json:"password"`
	Email     string  `json:"email"`
	Role      string  `json:"role"`
	Company   string  `json:"company"`
	Longitude float64 `json:"longitude"`
	Latitude  float64 `json:"latitude"`
	Fio       string  `json:"fio"`
	Phone     string  `json:"phone"`
	Country   string  `json:"country"`
	City      string  `json:"city"`
	UTC       int     `json:"utc"`
	Unn       string  `json:"unn"`
	Car       string  `json:"car"`
	Language  string  `json:"language"`
	Avatar    []byte  `json:"avatar"`
}

type SignResponse struct {
	Success      bool    `json:"success"`
	ErrorCode    int     `json:"errorCode,omitempty"`
	UserId       int     `json:"userId,omitempty"`
	Login        string  `json:"login,omitempty"`
	Email        string  `json:"email,omitempty"`
	Latitude     float64 `json:"latitude,omitempty"`
	Longitude    float64 `json:"longitude,omitempty"`
	AvatarUrl    string  `json:"avatarUrl,omitempty"`
	Car          string  `json:"car,omitempty"`
	AuthToken    string  `json:"authToken,omitempty"`
	RefreshToken string  `json:"refreshToken,omitempty"`
	Company      string  `json:"company,omitempty"`
	Role         string  `json:"role,omitempty"`
}

type SignInRequest struct {
	Login    string `json:"login"`
	Password string `json:"password"`
}

func ValidateSignUp(req SignUpRequest) int {

	if len(req.Login) == 0 {
		return ErrorLoginEmpty
	}

	if len(req.Password) == 0 {
		return ErrorPasswordEmpty
	}

	if len(req.Email) == 0 {
		return ErrorEmailEmpty
	}

	user := models.User{}
	if user.Find(req.Login) {
		return ErrorLoginExists
	}

	if user.Find(req.Email) {
		return ErrorEmailExists
	}
	return 0
}

func SignUp(c *gin.Context) {
	req := SignUpRequest{}
	c.Bind(&req)

	code := ValidateSignUp(req)
	if code != 0 {
		sendBadAnswer(c, code)
		return
	}

	u := models.User{}

	copier.Copy(&u, req)
	name := encryption.GeneratePassword(8)
	u.Password = encryption.GetHash(req.Password)
	u.AvatarUrl = config.AvatarUrl + name + ".jpg"
	u.Insert()

	if len(req.Avatar) != 0 {
		out, err := os.Create(config.AvatarPath + name + ".jpg")
		if err != nil {
			log.Fatal(err)
		}
		defer out.Close()
		out.Write(req.Avatar)
	} else {
		srcFile, _ := os.Open(config.AvatarPath + "empty.jpg")
		defer srcFile.Close()
		destFile, _ := os.Create(config.AvatarPath + name + ".jpg")
		defer destFile.Close()
		_, _ = io.Copy(destFile, srcFile)
		destFile.Sync()
	}
	//email.Registration(req.Email, req.Login, req.Password, req.Language)
	sendAnswer(u, c)
}

func SignIn(c *gin.Context) {
	req := SignInRequest{}
	c.Bind(&req)

	u := models.User{}
	if !u.Find(req.Login) {
		sendBadAnswer(c, ErrorLoginNotFound)
		return
	}

	err := bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(req.Password))
	if err != nil {
		sendBadAnswer(c, ErrorPasswordNotValid)
		return
	}
	sendAnswer(u, c)
}

func sendAnswer(u models.User, c *gin.Context) {
	res := SignResponse{}
	res.Success = true
	res.UserId = u.Id
	res.Email = u.Email
	res.Login = u.Login
	res.Latitude = u.Latitude
	res.Longitude = u.Longitude
	res.AvatarUrl = u.AvatarUrl
	res.Company = u.Company
	res.Role = u.Role
	res.AuthToken = "JWT " + encryption.GenerateAuthToken(u.Id, u.Login, u.Email, u.Role, u.Company)
	res.RefreshToken = "JWT " + encryption.GenerateRefreshToken(u.Id)
	wh := models.WhiteList{}
	wh.UserId = u.Id
	wh.Token = res.RefreshToken
	wh.Insert()
	c.JSON(http.StatusOK, res)
}

func sendBadAnswer(c *gin.Context, code int) {
	res := SignResponse{}
	res.Success = false
	res.ErrorCode = code
	c.JSON(http.StatusOK, res)
}

type RefreshRequest struct {
	RefreshToken string `json:"refreshToken"`
}

type RefreshResponse struct {
	AuthToken    string `json:"authToken"`
	RefreshToken string `json:"refreshToken"`
}

func Refresh(c *gin.Context) {
	strToken := c.GetHeader("Authorization")
	if len(strToken) == 0 {
		c.AbortWithStatus(http.StatusUnauthorized)
	}

	if strings.Contains(strToken, "JWT ") {
		strToken = strings.Trim(strToken, "JWT ")
	}

	token, err := jwt.Parse(strToken, func(token *jwt.Token) (interface{}, error) {
		return []byte(config.SecretToken), nil
	})

	if err != nil && err.Error() != "Token is expired" {
		fmt.Println(err)
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	if !token.Valid && err.Error() != "Token is expired" {
		fmt.Println("token no valid")
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	params, _ := json.Marshal(token.Claims.(jwt.MapClaims))
	info := encryption.TokenInfo{}
	json.Unmarshal([]byte(params), &info)

	req := RefreshRequest{}
	c.Bind(&req)

	u := models.User{}
	if !u.FindById(info.Id) {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	wl := models.WhiteList{}
	if !wl.Find(info.Id, req.RefreshToken) {
		c.AbortWithStatus(http.StatusUnauthorized)
		return
	}
	fmt.Println(req.RefreshToken)

	res := RefreshResponse{}
	res.AuthToken = "JWT " + encryption.GenerateAuthToken(u.Id, u.Login, u.Email, u.Role, u.Company)
	res.RefreshToken = "JWT " + encryption.GenerateRefreshToken(u.Id)
	wl.Update(res.RefreshToken)

	c.JSON(http.StatusOK, res)
}

type ChangePasswordRequest struct {
	PasswordNew string `json:"passwordNew"`
	PasswordOld string `json:"passwordOld"`
}

type ChangePasswordResponse struct {
	Success      bool   `json:"success"`
	Error        int    `json:"error,omitempty"` //1 - пользователь не найден 2 - неверный старый пароль
	AuthToken    string `json:"authToken,omitempty"`
	RefreshToken string `json:"refreshToken,omitempty"`
}

func ChangePassword(c *gin.Context) {
	params := encryption.GetUserParams(c)
	req := ChangePasswordRequest{}
	c.Bind(&req)
	res := ChangePasswordResponse{}
	res.Success = false

	user := models.User{}
	if !user.Find(params.Login) {
		res.Error = ErrorUserNotFound
		c.JSON(http.StatusOK, res)
		return
	}

	err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(req.PasswordOld))
	if err != nil {
		res.Error = ErrorOldPasswordNotValid
		c.JSON(http.StatusOK, res)
		return
	}

	user.UpdatePassword(encryption.GetHash(req.PasswordNew))

	wl := models.WhiteList{}
	wl.DeleteByUserId(params.Id)

	res.Success = true
	res.RefreshToken = encryption.GenerateRefreshToken(user.Id)
	res.AuthToken = encryption.GenerateAuthToken(user.Id, user.Login, user.Email, user.Role, user.Company)
	c.JSON(http.StatusOK, res)
}

type ChangeEmailRequest struct {
	NewEmail string `json:"newEmail"`
	Password string `json:"password"`
}

type ChangeEmailResponse struct {
	Success   bool   `json:"success"`
	Error     int    `json:"error,omitempty"` //1 - пользователь не найден 2 - неверный пароль 3 - email уже существует
	AuthToken string `json:"authToken,omitempty"`
}

func ChangeEmail(c *gin.Context) {
	params := encryption.GetUserParams(c)
	req := ChangeEmailRequest{}
	res := ChangeEmailResponse{}
	res.Success = false

	c.Bind(&req)

	user := models.User{}
	if !user.Find(params.Login) {
		res.Error = ErrorUserNotFound
		c.JSON(http.StatusOK, res)
		return
	}

	err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(req.Password))
	if err != nil {
		res.Error = ErrorPasswordNotValid
		c.JSON(http.StatusOK, res)
		return
	}

	if user.FindByEmail(req.NewEmail) {
		res.Error = ErrorEmailExists
		c.JSON(http.StatusOK, res)
		return
	}

	user.Email = req.NewEmail
	user.Update()
	res.Success = true
	res.AuthToken = encryption.GenerateAuthToken(user.Id, user.Login, user.Email, user.Role, user.Company)
	c.JSON(http.StatusOK, res)
}

type NewPasswordRequest struct {
	Login string `json:"login"`
}

type NewPasswordResponse struct {
	Success bool `json:"success"`
	Error   int  `json:"error"` // 1 - пользователь не найден 2 - ошибка отправки на почту
}

func NewPassword(c *gin.Context) {
	req := NewPasswordRequest{}
	c.Bind(&req)

	u := models.User{}

	res := NewPasswordResponse{}
	res.Success = false

	if !u.Find(req.Login) {
		res.Error = ErrorUserNotFound
		c.JSON(http.StatusOK, res)
		return
	}

	password := encryption.GeneratePassword(10)
	err := email.Send(u.Email, "Смена пароля Charge & GO", "Ваш новый пароль "+password)
	if err != nil {
		res.Error = ErrorSendEmail
		c.JSON(http.StatusOK, res)
		return
	}

	wl := models.WhiteList{}
	wl.DeleteByUserId(u.Id)

	res.Success = true
	password = encryption.GetHash(password)
	u.UpdatePassword(password)
	c.JSON(http.StatusOK, res)
}

type IsLoginExistsRequest struct {
	Login string `json:"login"`
}

type IsLoginExistsResponse struct {
	Exists bool `json:"exists"`
}

func IsLoginExists(c *gin.Context) {
	req := IsLoginExistsRequest{}
	c.Bind(&req)

	res := IsLoginExistsResponse{}
	res.Exists = false

	u := models.User{}
	if u.Find(req.Login) {
		res.Exists = true
	}

	c.JSON(http.StatusOK, res)
}

type ChangeAvatarRequest struct {
	Avatar []byte `json:"avatar"`
}

type ChangeAvatarResponse struct {
	Success   bool   `json:"success"`
	Error     int    `json:"error"`
	AvatarUrl string `json:"avatarUrl"`
}

func ChangeAvatar(c *gin.Context) {
	params := encryption.GetUserParams(c)
	req := ChangeAvatarRequest{}
	c.Bind(&req)

	res := ChangeAvatarResponse{}
	res.Success = false
	user := models.User{}
	if !user.Find(params.Login) {
		res.Error = ErrorUserNotFound
		c.JSON(http.StatusOK, res)
		return
	}

	name := encryption.GeneratePassword(8)

	if len(req.Avatar) != 0 {
		out, err := os.Create(config.AvatarPath + name + ".jpg")
		if err != nil {
			fmt.Println(err)
		}
		defer out.Close()
		out.Write(req.Avatar)
	}

	user.AvatarUrl = config.AvatarUrl + name + ".jpg"
	user.Update()
	res.AvatarUrl = user.AvatarUrl
	res.Success = true
	c.JSON(http.StatusOK, res)
}

type ChangeUserDataRequest struct {
	Company   string  `json:"company"`
	Longitude float64 `json:"longitude"`
	Latitude  float64 `json:"latitude"`
	Fio       string  `json:"fio"`
	Phone     string  `json:"phone"`
	Country   string  `json:"country"`
	City      string  `json:"city"`
	UTC       int     `json:"utc"`
	Unn       string  `json:"unn"`
	Car       string  `json:"car"`
	Language  string  `json:"language"`
}

type ChangeUserDataResponse struct {
	Success bool `json:"success"`
	Error   int  `json:"error"` //1 - пользователь не найден
}

func ChangeData(c *gin.Context) {
	params := encryption.GetUserParams(c)
	req := ChangeUserDataRequest{}
	res := ChangeUserDataResponse{}
	res.Success = false

	c.Bind(&req)

	user := models.User{}
	if !user.Find(params.Login) {
		res.Error = ErrorUserNotFound
		c.JSON(http.StatusOK, res)
		return
	}

	fmt.Println(req)
	copier.Copy(&user, req)
	user.Update()
	res.Success = true
	c.JSON(http.StatusOK, res)
}

type GetDataResponse struct {
	Success   bool    `json:"success"`
	Company   string  `json:"company"`
	Longitude float64 `json:"longitude"`
	Latitude  float64 `json:"latitude"`
	Fio       string  `json:"fio"`
	Phone     string  `json:"phone"`
	Country   string  `json:"country"`
	City      string  `json:"city"`
	UTC       int     `json:"utc"`
	Unn       string  `json:"unn"`
	Car       string  `json:"car"`
	Language  string  `json:"language"`
	AvatarUrl string  `json:"avatarUrl"`
	Email     string  `json:"email"`
}

func GetData(c *gin.Context) {
	params := encryption.GetUserParams(c)

	res := GetDataResponse{}
	user := models.User{}
	res.Success = user.Find(params.Login)
	if res.Success {
		copier.Copy(&res, user)
	}

	c.JSON(http.StatusOK, res)
}

type CreateNewUserResponse struct {
	Success   bool `json:"success"`
	ErrorCode int  `json:"errorCode"`
}

func CreateNewUser(c *gin.Context) {
	params := encryption.GetUserParams(c)
	req := SignUpRequest{}
	c.Bind(&req)
	res := CreateNewUserResponse{}
	res.Success = false

	code := ValidateSignUp(req)
	if code != 0 {
		res.ErrorCode = code
		c.JSON(http.StatusOK, res)
		return
	}

	if strings.ToLower(params.Role) != "admin" && strings.ToLower(params.Role) != "superadmin" {
		res.ErrorCode = ErrorNoPermission
		c.JSON(http.StatusOK, res)
		return
	}

	if strings.ToLower(params.Role) == "admin" && strings.ToLower(req.Role) != "oper" {
		res.ErrorCode = ErrorWrongRole
		c.JSON(http.StatusOK, res)
		return
	}

	if strings.ToLower(req.Role) != "admin" && strings.ToLower(req.Role) != "oper" {
		res.ErrorCode = ErrorWrongRole
		c.JSON(http.StatusOK, res)
		return
	}

	u := models.User{}

	copier.Copy(&u, req)
	u.ParentId = params.Id

	u.Password = encryption.GetHash(req.Password)
	name := encryption.GeneratePassword(8)
	u.AvatarUrl = config.AvatarUrl + name + ".jpg"
	u.Insert()

	srcFile, _ := os.Open(config.AvatarPath + "empty.jpg")
	defer srcFile.Close()
	destFile, _ := os.Create(config.AvatarPath + name + ".jpg")
	defer destFile.Close()
	_, _ = io.Copy(destFile, srcFile)
	destFile.Sync()

	res.Success = true
	c.JSON(http.StatusOK, res)
}

type GetUsersResponse struct {
	Success   bool   `json:"success"`
	ErrorCode int    `json:"errorCode"`
	Users     []User `json:"users"`
}

type User struct {
	Id        int    `json:"id"`
	Count     int    `json:"count"`
	Login     string `json:"login"`
	Company   string `json:"company"`
	Fio       string `json:"fio"`
	Phone     string `json:"phone"`
	AvatarUrl string `json:"avatarUrl"`
	Email     string `json:"email"`
	Role      string `json:"role"`
}

func GetUsers(c *gin.Context) {
	params := encryption.GetUserParams(c)
	res := GetUsersResponse{}
	res.Success = false

	if strings.ToLower(params.Role) != "admin" && strings.ToLower(params.Role) != "superadmin" {
		res.ErrorCode = ErrorNoPermission
		c.JSON(http.StatusOK, res)
		return
	}

	u := models.Users{}
	if !u.Find(params.Id) {
		c.JSON(http.StatusOK, res)
		return
	}
	copier.Copy(&res.Users, u)

	for i := range res.Users {
		res.Users[i].Count = models.GetStationsCount(res.Users[i].Id)
	}
	fmt.Println(res)

	res.Success = true
	c.JSON(http.StatusOK, res)
}

type DeleteUserRequest struct {
	UserId int `json:"userId"`
}

type DeleteUserResponse struct {
	Success   bool `json:"success"`
	ErrorCode int  `json:"errorCode"`
}

func DeleteUser(c *gin.Context) {
	params := encryption.GetUserParams(c)
	req := DeleteUserRequest{}
	c.Bind(&req)
	res := DeleteUserResponse{}
	res.Success = false

	if strings.ToLower(params.Role) != "admin" && strings.ToLower(params.Role) != "superadmin" {
		res.ErrorCode = ErrorNoPermission
		c.JSON(http.StatusOK, res)
		return
	}

	u := models.User{}
	if !u.FindById(req.UserId) {
		res.ErrorCode = ErrorUserNotFound
		c.JSON(http.StatusOK, res)
		return
	}

	if u.ParentId != params.Id {
		res.ErrorCode = ErrorNoPermission
		c.JSON(http.StatusOK, res)
		return
	}

	if strings.ToLower(u.Role) != "admin" && strings.ToLower(u.Role) != "oper" {
		res.ErrorCode = ErrorNoPermission
		c.JSON(http.StatusOK, res)
		return
	}

	u.Delete()
	res.Success = true
	c.JSON(http.StatusOK, res)
}
