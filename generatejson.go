package main

import (
	"./middleware"
	"encoding/json"
	"fmt"
)

func main() {
	j := middleware.SignUpRequest{}
	s, _ := json.Marshal(j)
	fmt.Println(string(s))
}
