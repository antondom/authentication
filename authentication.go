package main

import (
	"./router"
	"bitbucket.org/antondom/evchargerlibrary/config"
	"strconv"
)

func main() {
	r := router.New()
	p, _ := strconv.Atoi(config.AuthPort)
	p += 10
	go r.RunTLS(":"+strconv.Itoa(p), config.SSLCertificate, config.SSlKey)
	r.Run(":" + config.AuthPort)
}
